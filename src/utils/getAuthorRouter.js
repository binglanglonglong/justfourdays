const getRouter = (author, routers) => {
    let authorRouter = routers.filter(item => item.meta.author.indexOf(author) > -1);
    authorRouter.forEach(item => {
        if(item.children){
            item.children = getRouter(author, item.children)
        }
    })
    return authorRouter
}

export default getRouter