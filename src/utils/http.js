import axios from 'axios'
// import router from '../router';
import cookie from 'js-cookie'
//超时5s
axios.defaults.timeout = 5000;
//获取数据的地址  设定 defaults默认 baseURL默认地址
axios.defaults.baseURL = '/api';
//interceptors拦截 request请求 config是相关配置
axios.interceptors.request.use((config)=>{
    //如果地址是/user/login的话
    let token = cookie.get('token');//获取token
    if(token){
        config.headers.authorization = token//authorization授权，认可；批准，委任
    }
    // console.log(config)//返回值要么没有data，有么data为空
    return config
}, error => Promise.reject(error))  //如果拦截请求失败
axios.interceptors.response.use((response) => {//response 请求的信息配置等等   interceptors拦截器
    // console.log(response)//为什么返回失败呢？
    return response;
})
export default axios;