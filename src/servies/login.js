import axios from '../utils/http.js'

export function login(user_name,user_pwd) {
    return axios.post('/user/login',{   
        user_name: user_name,
        user_pwd: user_pwd
    })
}

export function getUserInfo() {
    return axios.get('/user/userInfo')
}