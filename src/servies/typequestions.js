import axios from '../utils/http.js'

export function Typequestion(){
    return axios.get('/exam/getQuestionsType')
}

export function insertQuestionsType(text,sort){
    return axios.get(`/exam/insertQuestionsType?text=${text}&sort=${sort}`)
}

export function subject(){
    return axios.get('/exam/subject')
}

export function questionslist(){
    return axios.get('/exam/questions/new')
}


export function condition(exam_name,questions_type_text){
    return axios.get(`/exam/questions/condition?exam_id=${exam_name}&questions_type_id=${questions_type_text}`)
}