import axios from '../utils/http.js'

export function clas(){
    return axios.get('/manger/grade')
}

export function rooms(){
    return axios.get('/manger/room')
}

export function addrooms(room_text){
    return axios.post('/manger/room',{room_text})
}

export function deleterooms(room_id){
    return axios.delete('/manger/room/delete',{data:{room_id}})
}

export function studentlist(){
    return axios.get('/manger/student')
}

export function deletestudent(student_id){
    return axios.delete('/manger/student/:id=>student_id',{params:{id:student_id}})
}

export function addclas(grade_name,room_id,subject_id){
    return axios.post('/manger/grade',{grade_name,room_id,subject_id})
}   

export function updateclas(grade_name,grade_id,subject_id){
    return axios.put('/manger/grade/update',{grade_name,grade_id,subject_id})
}

export function deleteclas(grade_id){
    return axios.delete('/manger/grade/delete',{data:{grade_id}})
}   