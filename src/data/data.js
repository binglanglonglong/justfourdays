let options = [
  {
    id: 0,
    text: '用户数据',
    url: '/user/user',
    children: [
      {
        id: 'user_name',
        text: '用户名'
      },
      {
        id: 'user_pwd',
        text: '密码'
      },
      {
        id: 'identity_text',
        text: '身份'
      }
    ]
  },
  {
    id: 1,
    text: '身份数据',
    url: '/user/identity',
    children: [
      {
        id: 'identity_id',
        text: '身份数据'
      }
    ]
  },
  {
    id: 2,
    text: 'api接口权限',
    url: '/user/api_authority',
    children: [
      {
        id: 'api_authority_text',
        text: 'api权限名称'
      },
      {
        id: 'api_authority_url',
        text: 'api权限url'
      },
      {
        id: 'api_authority_method',
        text: 'api权限方法'
      }
    ]
  },
  {
    id: 3,
    text: '身份和api接口关系',
    url: '/user/identity_api_authority_relation',
    children: [
      {
        id: 'identity_text',
        text: '身份名称'
      },
      {
        id: 'api_authority_text',
        text: 'api权限名称'
      },
      {
        id: 'api_authority_url',
        text: 'api权限url'
      },
      {
        id: 'api_authority_method',
        text: 'api权限方法'
      }
    ]
  },
  {
    id: 4,
    text: '视图接口权限',
    url: '/user/view_authority',
    children: [
      {
        id: 'view_authority_text',
        text: '视图权限名称'
      },
      {
        id: 'view_id',
        text: '视图id'
      }
    ]
  },
  {
    id: 5,
    text: '身份和视图权限关系',
    url: '/user/identity_view_authority_relation',
    children: [
      {
        id: 'identity_text',
        text: '身份'
      },
      {
        id: 'view_authority_text',
        text: '视图名称'
      },
      {
        id: 'view_id',
        text: '视图id'
      }
    ]
  }
];

export default options;
