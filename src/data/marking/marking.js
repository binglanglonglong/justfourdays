let options = [
  {
    id: 'grade_name',
    text: '班级'
  },
  {
    id: 'student_name',
    text: '姓名'
  },
  {
    id: 'status',
    text: '阅卷状态'
  },
  {
    id: 'start_time',
    text: '开始时间'
  },
  {
    id: 'end_time',
    text: '结束时间'
  },
  {
    id: 'z',
    text: '成才率'
  }
];

export default options;
