let subject = [
  {
    value: '0',
    label: 'javaScript上'
  },
  {
    value: '1',
    label: 'javaScript下'
  },
  {
    value: '2',
    label: '模块化开发'
  },
  {
    value: '3',
    label: '移动端开发'
  },
  {
    value: '4',
    label: 'node基础'
  },
  {
    value: '5',
    label: '组件化开发'
  },
  {
    value: '6',
    label: '渐进式开发'
  },
  {
    value: '7',
    label: '项目实战'
  },
  {
    value: '8',
    label: 'javaScript高级'
  },
  {
    value: '9',
    label: 'node高级'
  }
];

export default subject;
