let options = [
    {
      id:"title",
      text: '试卷信息',
    },
    {
      id:"grade_name",
      text: '班级',
    },
    {
      id:"user_name",
      text: '创建人',
    },
    {
      id:"start_time",
      text: '开始时间',
    },
    {
      id:"end_time",
      text: '结束时间',
    }
  ];
  
  export default options;
  