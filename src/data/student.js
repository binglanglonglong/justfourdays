let options = [
    {
      id:"student_name",
      text: '姓名',
    },
    {
      id:"student_id",
      text: '学号',
    },
    {
      id:"grade_name",
      text: '班级',
    },
    {
      id:"room_text",
      text: '教室',
    },
    {
      id:"student_pwd",
      text: '密码',
    }
  ];
  
  export default options;