import getRouter from '../utils/getAuthorRouter'
import routers from '../router/home.js'
import Router from '../router/index'

import {login,getUserInfo} from '../servies/login.js'


const state = {
    user: '',
    routerss: []
}

const mutations = {
    //当前用户信息添加
    at_present_user(state,n){
       state.user = n;
    },
    change_router(state,n){
        state.routerss = n
    },
    remove_router(state){
        state.routerss = []
    }
}


const actions = {
    //获取数据及处理
    async getData(context){
        //await 等待
        // let res = await login()
        // window.sessionStorage.setItem('token',JSON.stringify(res))
        
    },
    async getUserInfo({dispatch,commit}) {
        let res = await getUserInfo();
        window.sessionStorage.setItem('userinfo',res.data.data.identity_text);
        let resRouter = getRouter(res.data.data.identity_text ,routers);
        commit('change_router',resRouter)
        Router.addRoutes(resRouter)
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}











