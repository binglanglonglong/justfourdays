import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);



let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/login/login.vue')
    }
  ]
});

let protect=['/','/addquestions','/examdeteil','/marking','/typequestions','/adduser','/Exhibitionuser','/class','/classroom','/student','/addexamination','/examinationlist','/classlist','/detail',];
router.beforeEach((to,from,next)=>{
  if(protect.includes(to.path)){
    let token= JSON.parse(window.sessionStorage.getItem('token'))
    if(token){
      next()
    }else{
      next('/login')
    }
  }else{
    next()
  }
})
export default router
