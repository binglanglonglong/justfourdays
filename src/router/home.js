import Home from '../views/Home.vue';
 
export default [
    {
      path: '/',
      name: 'home',
      meta: {
        author: ['出题者','浏览者','管理员']
      },
      component: Home,
      children: [
        {
          path: '/addquestions',
          name: 'addquestions',
          meta: {
            author: ['管理员']
          },
          component: () => import('../views/questions/addquestions.vue')
        },
        {
          path: '/seequestions',
          name: 'seequestions',
          meta: {
            author: ['管理员']
          },
          component: () => import('../views/questions/seequestions.vue')
        },
        {
          path: '/typequestions',
          name: 'typequestions',
          meta: {
            author: ['管理员']
          },
          component: () => import('../views/questions/typequestions.vue')
        },
        {
          path: '/adduser',
          name: 'adduser',
          meta: {
            author: ['管理员']
          },
          component: () => import('../views/user/adduser.vue')
        },
        {
          path: '/Exhibitionuser',
          name: 'Exhibitionuser',
          meta: {
            author: ['出题者']
          },
          component: () => import('../views/user/Exhibitionuser.vue')
        },
        {
          path: '/class',
          name: 'class',
          meta: {
            author: ['浏览者']
          },
          component: () => import('../views/class/class.vue')
        },
        {
          path: '/classroom',
          name: 'classroom',
          meta: {
            author: ['出题者','浏览者']
          },
          component: () => import('../views/class/classroom.vue')
        },
        {
          path: '/student',
          name: 'student',
          meta: {
            author: ['出题者','浏览者']
          },
          component: () => import('../views/class/student.vue')
        },
        {
          path: '/addexamination',
          name: 'addexamination',
          meta: {
            author: ['出题者','浏览者']
          },
          component: () => import('../views/examination/addexamination.vue')
        },
        {
          path: '/examinationlist',
          name: 'examinationlist',
          meta: {
            author: ['出题者','浏览者']
          },
          component: () => import('../views/examination/examinationlist.vue')
        },
        {
          path: '/classlist',
          name: 'classlist',
          meta: {
            author: ['出题者','浏览者']
          },
          component: () => import('../views/marking/classlist.vue')
        },
        {
          path: '/detail',
          name: 'detail',
          meta: {
            author: ['出题者','浏览者']
          },
          component: () => import('../views/questions/detail.vue')
        },
        {
          path: '/marking',
          name: 'marking',
          meta: {
            author: ['出题者','浏览者']
          },
          component: () => import('../views/marking/marginking.vue')
        },
        {
          path: '/examdeteil',
          name: 'examdeteil',
          meta: {
            author: ['出题者','浏览者']
          },
          component: () => import('../views/examination/examdeteil.vue')
        }
      ]
    }
  ];
